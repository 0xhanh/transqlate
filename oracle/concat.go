package oracle

import (
	"strings"

	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

type rewriteConcatOp struct{}

func (rewriteConcatOp) String() string {
	return "use CONCAT instead of || operator"
}

func (rewriteConcatOp) Match(n ast.Node) bool {
	g, ok := n.(ast.Grouping)
	if !ok {
		return false
	}

	if g.Items[0].Token.Raw != "||" {
		return false
	}

	// Skip rewrite if items are not identifiers
	for _, item := range g.Items {
		l, ok := item.Expression.(ast.Leaf)
		if !ok || l.Token.Type == lexer.Identifier {
			continue
		}
		return false
	}

	return true
}

func (rewriteConcatOp) Rewrite(n ast.Node) (ast.Node, error) {
	g := n.(ast.Grouping)
	l := len(g.Items)
	items := []ast.Postfix{}
	comma := lexer.MustLex(",")[0]
	call := parser.MustParse("concat()").(ast.Call)

	for i, item := range g.Items {
		// Move first item prefix to opening parenthesis
		if i == 0 {
			item, call = ast.SwapPrefixes(item, call)
		}

		// Preserve item suffixes except for last item
		if i < l-1 {
			comma = comma.SpaceFrom(item.Token)

			// Remove leading space from comma
			comma.Prefix = strings.TrimPrefix(comma.Prefix, " ")

			item = ast.Postfix{
				Expression: item.Expression,
				Token:      comma,
			}
		}

		// Move last item suffix to closing parenthesis
		if i == l-1 {
			item, call = ast.SwapSuffixes(item, call)
		}

		items = append(items, item)
	}

	call.Args.Items = items
	return call, nil
}
