package oracle_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestConcatRewrite(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "a || b")
	r.NoError(err)
	r.Equal("concat(a, b)", sql)

	sql, err = oracle.Translate("<stdin>", "foo(a) || b")
	r.NoError(err)
	r.Equal("concat(foo(a), b)", sql)
}

func TestConcatMultiple(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "a || b || c")
	r.NoError(err)
	r.Equal("concat(a, b, c)", sql)
}

func TestConcatIndent(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT a ||
		   b ||
		   c
	FROM c
	`))

	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT concat(a,
		   b,
		   c)
	FROM c
	`), sql)
}

func TestConcatWithComment(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "a || b -- comment")
	r.NoError(err)
	r.Equal("concat(a, b) -- comment", sql)

	sql, err = oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT a -- comment
		|| b -- comment
	FROM c
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT concat(a -- comment
		, b) -- comment
	FROM c
	`), sql)
}

func TestConcatSkipRewriteWithoutIdentifiers(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "'a' || 'b'")
	r.NoError(err)
	r.Equal("'a' || 'b'", sql)
}
