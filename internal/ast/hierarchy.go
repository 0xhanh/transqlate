package ast

import "gitlab.com/dalibo/transqlate/lexer"

// ConnectBy holds CONNECT BY clause of SELECT statement.
type ConnectBy struct {
	ConnectBy []lexer.Token
	Condition Node
	StartWith Node
}

func (c ConnectBy) Tokens() []lexer.Token {
	t := c.ConnectBy
	t = append(t, c.Condition.Tokens()...)
	if c.StartWith != nil {
		t = append(t, c.StartWith.Tokens()...)
	}
	return t
}

func (c ConnectBy) TransformStart(f Transformer[lexer.Token]) Node {
	c.ConnectBy[0] = f(c.ConnectBy[0])
	return c
}

func (c ConnectBy) TransformEnd(f Transformer[lexer.Token]) Node {
	if c.StartWith == nil {
		c.Condition = c.Condition.TransformEnd(f)
	} else {
		c.StartWith = c.StartWith.TransformEnd(f)
	}
	return c
}

func (c ConnectBy) Visit(f Transformer[Node]) Node {
	c.Condition = c.Condition.Visit(f)
	if c.StartWith != nil {
		c.StartWith = c.StartWith.Visit(f)
	}
	return f(c)
}

type StartWith struct {
	StartWith []lexer.Token
	Condition Node
	ConnectBy Node
}

func (s StartWith) Tokens() []lexer.Token {
	t := s.StartWith
	t = append(t, s.Condition.Tokens()...)
	if s.ConnectBy != nil {
		t = append(t, s.ConnectBy.Tokens()...)
	}
	return t
}

func (s StartWith) TransformStart(f Transformer[lexer.Token]) Node {
	s.StartWith[0] = f(s.StartWith[0])
	return s
}

func (s StartWith) TransformEnd(f Transformer[lexer.Token]) Node {
	if s.ConnectBy == nil {
		s.Condition = s.Condition.TransformEnd(f)
	} else {
		s.ConnectBy = s.ConnectBy.TransformEnd(f)
	}
	return s
}

func (s StartWith) Visit(f Transformer[Node]) Node {
	s.Condition = s.Condition.Visit(f)
	if s.ConnectBy != nil {
		s.ConnectBy = s.ConnectBy.Visit(f)
	}
	return f(s)
}
