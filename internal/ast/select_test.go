package ast_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestSelectScalar(t *testing.T) {
	r := require.New(t)
	var s ast.Node = ast.Select{
		Select: lexer.MustLex("SELECT"),
		List:   parser.MustParse(" SYSDATE"),
	}
	sql := lexer.Write(s)
	r.Equal("SELECT SYSDATE", sql)
}

func TestSelectFrom(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT SYSDATE FROM DUAL").(ast.Select)
	r.Equal("SELECT", s.Select[0].Raw)
	r.Equal("SYSDATE", s.List.(ast.Leaf).Token.Raw)
	r.Equal("DUAL", s.From.Tables.Items[0].Expression.(ast.Leaf).Token.Raw)
	sql := lexer.Write(s)
	r.Equal("SELECT SYSDATE FROM DUAL", sql)
}

func TestSelectAll(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT ALL SYSDATE FROM DUAL").(ast.Select)
	r.Equal("SELECT", s.Select[0].Raw)
	r.Equal("ALL", s.Select[1].Raw)
	sql := lexer.Write(s)
	r.Equal("SELECT ALL SYSDATE FROM DUAL", sql)
}

func TestSelectStar(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT * FROM a").(ast.Select)
	r.True(s.IsStar())

	s = parser.MustParse("SELECT b.* FROM b").(ast.Select)
	r.False(s.IsStar())
}

func TestSelectColumns(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse(dedent.Dedent(`
	SELECT
		a, a AS b, a.c,
		a.*, *,
		(SELECT 'anonymous'),
		array_agg(col)
	`)).(ast.Select)
	columns := s.ColumnNames()
	r.Len(columns, 7)
	r.Equal("a", columns[0])
	r.Equal("b", columns[1])
	r.Equal("c", columns[2])
	r.Equal("*", columns[3])
	r.Equal("*", columns[4])
	r.Equal("", columns[5])
	r.Equal("", columns[6])
}
