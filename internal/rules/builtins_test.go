package rules_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/internal/rules"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestAbsCase(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("ABS(1)")
	rule := rules.QuoteBuiltins{}
	r.True(rule.Match(n))
	n, err := rule.Rewrite(n)
	r.NoError(err)
	r.Equal(`"abs"(1)`, lexer.Write(n))
}
