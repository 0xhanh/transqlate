package internal

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log/slog"
	"os"
	"strings"

	"github.com/mattn/go-isatty"
	"github.com/mgutz/ansi"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/internal/render"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/oracle"
	"gitlab.com/dalibo/transqlate/rewrite"
)

var (
	verbose      bool
	showVersion  bool
	plain        bool
	plainLog     bool
	pretty       bool
	preserveCase bool
	source       string
)

// Transqlate CLI entrypoint
func Transqlate(version string) {
	exitCode := 0
	parseFlags()
	setupLogging()

	ansi.DisableColors(plainLog)
	if showVersion {
		fmt.Printf("transqlate %s\n", version)
		return
	}

	slog.Debug("Running transqlate.", "version", version)
	sql := readSource(source)
	slog.Debug("Parsing SQL.", "source", source)

	engine := buildEngine()
	if verbose {
		parser.StartTracing()
	}
	tree, err := engine.Parse(source, sql)
	render.ParserTrace()
	parser.StopTracing()
	if err != nil {
		fatal(err)
	}

	if verbose {
		slog.Debug("Dumping AST.", "dialect", "oracle")
		os.Stderr.WriteString(render.Ast(tree))
	}

	slog.Info("Translating SQL.", "engine", engine)
	tokens, err := engine.Run(tree)
	if err != nil {
		if tokens == nil { // Something went totaly wrong.
			fatal(err)
		}
		var errs joinedErrors
		if !errors.As(err, &errs) {
			slog.Warn(err.Error())
		}
		for _, e := range errs.Unwrap() {
			exitCode = 1
			var rerr rewrite.Err
			if !errors.As(e, &rerr) {
				slog.Warn(e.Error())
				continue
			}

			// Get the first and last token of the affected node.
			t, _ := lexer.Edges(rerr.Node)
			slog.Warn(
				"Rewrite failure.",
				"rule", rerr.Rule,
				"err", e,
				"line", 1+t.Line,
				"column", 1+t.Column,
			)
		}
		// Don't Fatal here, we still have a partial AST.
	}

	slog.Debug("Serialize SQL.", "dialect", "postgres")
	tokens = render.Renumber(lexer.Token{}, tokens)
	tokens = render.CommentErrors(tokens)
	ansi.DisableColors(plain)
	os.Stdout.WriteString(lexer.Write(render.Highlight(tokens)))
	os.Exit(exitCode)
}

func buildEngine() rewrite.Engine {
	engine := oracle.Engine(
		oracle.DefaultDateFormat("YYYY-MM-DD"),
		oracle.PreserveCase(preserveCase),
	)
	if pretty {
		engine.Apply(rewrite.Pretty())
	}
	return engine
}

func parseFlags() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage: %s [options] [<file>]\n", os.Args[0])
		fmt.Fprintf(flag.CommandLine.Output(), `
Transpile any snippet of SQL from Oracle to PostgreSQL.

Use - to read from stdin. This is the default.
Use @ to use a builtin sample.

Options:

`)
		flag.PrintDefaults()
		fmt.Fprintf(flag.CommandLine.Output(), `
See https://gitlab.com/dalibo/transqlate for more information and report issues.
Licensed under PostgreSQL license by Dalibo and contributors.
`)
	}

	if strings.HasPrefix(os.Args[0], "/tmp/go-build") || strings.Contains(os.Args[0], "__debug_bin") {
		// Default to verbose mode with go run
		verbose = true
		pretty = true
	}
	plain = !isatty.IsTerminal(os.Stdout.Fd())
	defaultPlain := plain

	flag.BoolVar(&plain, "plain", plain, "disable color")
	flag.BoolVar(&verbose, "v", verbose, "verbose mode")
	flag.BoolVar(&showVersion, "version", false, "show version")
	flag.BoolVar(&pretty, "pretty", pretty, "prettify SQL")
	flag.BoolVar(&preserveCase, "preserve-case", preserveCase, "assume object are migrated in Oracle case")
	flag.Parse()

	plainLog = !isatty.IsTerminal(os.Stderr.Fd())
	forcedPlain := !defaultPlain && plain
	if forcedPlain {
		plainLog = plain
	}

	source = flag.Arg(0)
	if source == "" {
		source = "-"
	}
}

func readSource(name string) string {
	switch name {
	case "-":
		if isatty.IsTerminal(os.Stdin.Fd()) {
			os.Stderr.WriteString("Type SQL snippet and terminate with ^D\n")
		}
		return readFile(os.Stdin)
	case "@":
		slog.Info("Using builtin sample.")
		sql := `-- Builtin sample
SELECT decode(col_param,
  1, TRUNC(
	NVL(col_date, SYSDATE),
	'YYYY'
  ),
  -- always use sysdate
  2, TRUNC(SYSDATE, 'YYYY')
) FROM t1, t2
WHERE t2.id = t1.t2_id(+);
`
		os.Stderr.WriteString(sql)
		return sql
	default:
		slog.Debug("Opening file.", "path", name)
		fd, err := os.Open(name)
		if err != nil {
			fatal(err)
		}
		defer fd.Close()
		return readFile(fd)
	}
}

func readFile(fd io.Reader) string {
	b := strings.Builder{}
	br := bufio.NewReader(fd)
	for {
		chunk, _, err := br.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			fatal(err)
		}
		b.Write(chunk)
		b.WriteByte('\n')
	}
	return b.String()
}

func fatal(o any) {
	err := o.(error)
	fmt.Fprintf(os.Stderr, "%s\n", err.Error())
	var lerr lexer.Err
	if !errors.As(err, &lerr) {
		os.Exit(1)
	}

	// Print errored line
	lineTokens := lerr.Tokens()
	if plainLog {
		width := len(lerr.BadToken().Raw)
		fmt.Fprintf(os.Stderr, "%s\n", render.Carret(lexer.Write(lineTokens), lerr.Column, width))
	} else {
		ansi.DisableColors(plainLog)
		fmt.Fprintf(os.Stderr, "%s\n", lexer.Write(render.Highlight(lineTokens)))
	}

	os.Exit(1)
}

type joinedErrors interface {
	Unwrap() []error
}
