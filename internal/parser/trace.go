package parser

// This file holds code to trace parser states recursion
//
// Go does not offer call tracing. Thus we implement a simple event collector explicitly called by
// some methods of the parser : consume and all parse* methods.
//
// A trace event is a simple string of the format <type>: <message>
// type can be enter or leave to trace recursion.
// type is consume to trace token consumption. Message for consume event is token type and raw token string.

import (
	"fmt"
	"runtime"
	"strings"
)

// Trace holds the parser messages.
//
// Use Trace.Events() []string to get events.
var Trace Logger = &voidLog{}

// StartTracing enables parser event collect.
//
// Use Trace.Events() to get events.
func StartTracing() {
	Trace = &memLog{}
}

func StopTracing() {
	Trace = &voidLog{}
}

// Logger is the interface for the trace logger.
type Logger interface {
	// Events return the list of parse trace events.
	Events() []string

	logf(string, ...interface{})
	// Returns the leave function
	enter() func()
	enterf(string, ...interface{}) func()
	leave()
}

type voidLog struct{}

func (l *voidLog) logf(format string, args ...interface{})          {}
func (l *voidLog) enter() func()                                    { return l.leave }
func (l *voidLog) enterf(format string, args ...interface{}) func() { return l.leave }
func (l *voidLog) leave()                                           {}
func (l *voidLog) Events() []string                                 { return nil }

type memLog struct {
	stack  []string
	events []string
}

func (l *memLog) enter() func() {
	l.stack = append([]string{callerFunction(1)}, l.stack...)
	l.logf("enter: %s", l.stack[0])
	return l.leave
}

func (l *memLog) enterf(format string, args ...interface{}) func() {
	caller := callerFunction(1)
	args = append([]interface{}{caller}, args...)
	l.stack = append([]string{fmt.Sprintf("%s "+format, args...)}, l.stack...)
	l.logf("enter: %s", l.stack[0])
	return l.leave
}

// callerFunction returns the name of the caller.
func callerFunction(skip int) string {
	pc, _, _, ok := runtime.Caller(1 + skip)
	if !ok {
		return "*unknown*"
	}
	frames := runtime.CallersFrames([]uintptr{pc})
	f, more := frames.Next()
	if more {
		panic("more")
	}
	point := strings.LastIndex(f.Function, ".")
	return f.Function[point+1:]
}

func (l *memLog) leave() {
	l.logf("leave: %s", l.stack[0])
	l.stack = l.stack[1:]
}

func (l *memLog) logf(format string, args ...interface{}) {
	l.events = append(l.events, fmt.Sprintf(format, args...))
}

func (l *memLog) Events() []string {
	return l.events
}
