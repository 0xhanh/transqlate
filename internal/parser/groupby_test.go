package parser_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestGroupBy(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM t GROUP BY 1")
	s := n.(ast.Select)
	r.NotNil(s.GroupBy)

	n = parser.MustParse("SELECT 1 FROM t GROUP BY ALL 1")
	s = n.(ast.Select)
	r.NotNil(s.GroupBy)

	n = parser.MustParse("SELECT 1 FROM t GROUP BY DISTINCT 1")
	s = n.(ast.Select)
	r.NotNil(s.GroupBy)
}

func TestHaving(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM t HAVING 2 GROUP BY 3")
	s := n.(ast.Select)
	h := s.GroupBy.(ast.Having)
	r.NotNil(h.GroupBy)

	n = parser.MustParse("SELECT 1 FROM t GROUP BY 2 HAVING 3")
	s = n.(ast.Select)
	g := s.GroupBy.(ast.GroupBy)
	r.NotNil(g.Having)
}
