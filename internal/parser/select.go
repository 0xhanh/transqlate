package parser

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// parseSelect parses a SELECT statement.
func (p *state) parseSelect() (ast.Node, error) {
	defer Trace.enter()()

	var err error
	var select_ ast.Select

	if p.keywords("WITH") {
		select_.With, err = p.parseWith()
		if err != nil {
			return nil, p.errorf("with: %w", err)
		}
	}

	if !p.keywords("SELECT") {
		return nil, p.unexpected()
	}

	p.keywords("", "ALL")
	select_.Select = p.consumeMatched()

	if p.keywords("DISTINCT") {
		select_.Distinct, err = p.parseDistinct()
		if err != nil {
			return nil, err
		}
	}

	g, err := p.parseItems(nil)
	if err != nil {
		return nil, err
	}
	if len(g.Items) == 1 {
		// Put scalar expression directly in the select node.
		// e.g. SELECT *
		select_.List = g.Items[0].Expression
	} else {
		select_.List = g
	}

	if p.keywords("FROM") {
		select_.From, err = p.parseFrom()
		if err != nil {
			return nil, p.errorf("from: %w", err)
		}
	}
	if p.keywords("WHERE") {
		select_.Where, err = p.parseWhere()
		if err != nil {
			return nil, p.errorf("where: %w", err)
		}
	}
	if p.keywords("START", "WITH") {
		select_.Hierarchy, err = p.parseStartWith()
		if err != nil {
			return nil, p.errorf("start with: %w", err)
		}
	}
	if p.keywords("CONNECT", "BY") {
		select_.Hierarchy, err = p.parseConnectBy()
		if err != nil {
			return nil, p.errorf("connect by: %w", err)
		}
	}

	// Accept Oracle HAVING before GROUP BY
	if p.keywords("HAVING") {
		select_.GroupBy, err = p.parseHaving()
		if err != nil {
			return nil, p.errorf("having: %w", err)
		}
	}

	if p.keywords("GROUP", "BY") {
		select_.GroupBy, err = p.parseGroupBy()
		if err != nil {
			return nil, p.errorf("group by: %w", err)
		}
	}

	if p.keywords("ORDER", "BY") || p.keywords("ORDER", "SIBLINGS", "BY") {
		select_.OrderBy, err = p.parseOrderBy()
		if err != nil {
			return nil, p.errorf("order by: %w", err)
		}
	}

	if p.keywords("LIMIT") {
		select_.Limit, err = p.parseLimit()
		if err != nil {
			return nil, p.errorf("limit: %w", err)
		}
	} else if p.keywords("OFFSET") {
		select_.Limit, err = p.parseOffset()
		if err != nil {
			return nil, p.errorf("offset: %w", err)
		}
	} else if p.keywords("FETCH") { // PostgreSQL accepts FETCH before OFFSET.
		select_.Limit, err = p.parseFetch()
		if err != nil {
			return nil, p.errorf("fetch: %w", err)
		}
	}

	if p.keywords("MINUS") || p.keywords("EXCEPT") || p.keywords("UNION") || p.keywords("INTERSECT") {
		// Match ALL or DISTINCT after set operator.
		p.keywords("", "ALL")
		p.keywords("", "DISTINCT")
		return p.parseInfix(select_, 0)
	}

	return select_, nil
}

func (p *state) parseDistinct() (d ast.Clause, err error) {
	defer Trace.enter()()
	if !p.keywords("DISTINCT", "ON") {
		d.Keywords = p.consumeMatched()
		return
	}
	d.Keywords = p.consumeMatched() // Consume also ON
	d.Expression, err = p.parseGrouping()
	return
}

// parseFrom parses a FROM clause.
func (p *state) parseFrom() (ast.From, error) {
	defer Trace.enter()()

	from := ast.From{
		From: p.consume(),
	}
	for {
		expr, err := p.parseAliasedExpression()
		if err != nil {
			return from, err
		}
		item := ast.Postfix{Expression: expr}
		if p.punctuations(",") {
			item.Token = p.consume()
		}
		from.Tables.Items = append(from.Tables.Items, item)

		if item.Token.IsZero() && !p.keywords("JOIN") && !p.keywords("", "JOIN") && !p.keywords("", "", "JOIN") && !p.keywords("NATURAL") {
			break
		}
	}

	return from, nil
}

// parseAliasedExpression
//
// Accept explicit AS alias or simple identifier.
// Use it in SELECT list, FROM list, JOIN table expression.
func (p *state) parseAliasedExpression() (expr ast.Node, err error) {
	defer Trace.enter()()
	expr, err = p.parseExpression(0)
	if err != nil {
		return
	}
	if p.keywords("AS") || p.seek(lexer.Identifier) {
		expr, err = p.parseAlias(expr, 0)
	}
	return
}

// parseJoin parses a JOIN expression
func (p *state) parseJoin() (ast.Node, error) {
	defer Trace.enter()()

	join := ast.Join{
		Join: p.consumeMatched(),
	}
	if join.Join[0].Str == "NATURAL" {
		if !p.keywords("JOIN") && !p.keywords("", "JOIN") && !p.keywords("", "", "JOIN") {
			return nil, p.unexpected()
		}
		join.Join = append(join.Join, p.consumeMatched()...)
	}
	right, err := p.parseAliasedExpression()
	if err != nil {
		return nil, err
	}
	join.Right = right

	var condition ast.Node
	if p.keywords("ON") {
		condition, err = p.parseWhere()
		if err != nil {
			return nil, fmt.Errorf("on: %w", err)
		}

	} else if p.keywords("USING") {
		using := p.consume()
		expr, err := p.parseGrouping()
		if err != nil {
			return nil, fmt.Errorf("using: %w", err)
		}
		condition = ast.Prefix{
			Token:      using,
			Expression: expr,
		}
	}
	join.Condition = condition

	return join, nil
}

// parseWhere parses a WHERE clause.
func (p *state) parseWhere() (ast.Where, error) {
	defer Trace.enter()()

	where := ast.Where{
		Keyword: p.consume(), // Assume WHERE or ON is matched
	}

	c, err := p.parseExpression(0)
	if err != nil {
		return ast.Where{}, err
	}
	// Split ANDs as a list of Prefix <AND right>. The tip of the tree is the *last* AND.
	i, _ := c.(ast.Infix)
	for i.Is("AND") {
		oneSideAnd := ast.Prefix{Token: i.Op[0], Expression: i.Right}
		where.Conditions = append([]ast.Prefix{oneSideAnd}, where.Conditions...)
		c = i.Left // Chain backward.
		i, _ = i.Left.(ast.Infix)
	}
	// Add the first non-AND condition.
	where.Conditions = append([]ast.Prefix{{Expression: c}}, where.Conditions...)
	return where, nil
}
