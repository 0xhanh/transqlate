package joinmark_test

import (
	"log/slog"
	"os"
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestMain(m *testing.M) {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))
	os.Exit(m.Run())
}

func TestLeft(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT *\nFROM table1 t1, table2 t2 WHERE t1.col1 = t2.col3 (+);")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT *
	FROM table1 t1
	LEFT OUTER JOIN table2 t2 ON t1.col1 = t2.col3;`)[1:], sql)
}

func TestRight(t *testing.T) {
	r := require.New(t)
	// Also checks for other operator.
	// Also checks schema and alias.
	// Also checks with uppercased identifiers.
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT *
	FROM SCHEMA.A, SCHEMA.A B
	WHERE "A"."COL"(+) > "B"."COL" AND 1=1;`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT *
	FROM SCHEMA.A
	RIGHT OUTER JOIN SCHEMA.A B ON A.COL > B.COL
	WHERE 1=1;`), sql)
}

func TestMixedStandardJoin(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM a LEFT OUTER JOIN b ON a.col = b.col, c WHERE b.col = c.col(+);")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT * FROM a
	LEFT OUTER JOIN b ON a.col = b.col
	LEFT OUTER JOIN c ON b.col = c.col;`)[1:], sql)
}

func TestKeepCrossJoin(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM a, b, c WHERE a.col = b.col(+);")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT * FROM a
	LEFT OUTER JOIN b ON a.col = b.col,
	c;`)[1:], sql)
}

func TestPreferLeft(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT 1 FROM b, a WHERE b.col(+) = a.col;")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT 1 FROM a
	LEFT OUTER JOIN b ON b.col = a.col;`)[1:], sql)
}

func TestPreferLeftOnce(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT 1 FROM b, a, c WHERE b.col(+) = a.col AND b.col(+) = c.col;")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT 1 FROM a
	LEFT OUTER JOIN b ON b.col = a.col
	RIGHT OUTER JOIN c ON b.col = c.col;`)[1:], sql)
}

func TestTableOrderPreserved(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT *
	FROM jobs, users, depts
	WHERE depts.id = users.deptid(+) AND users.jobid = jobs.id(+);
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT *
	FROM jobs
	RIGHT OUTER JOIN users ON users.jobid = jobs.id
	RIGHT OUTER JOIN depts ON depts.id = users.deptid;
	`), sql)
}

func TestSwappedCondition(t *testing.T) {
	r := require.New(t)
	// (+) is on left.
	// Also checks AS.
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM a, a AS b WHERE b.col(+) = a.col;")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT * FROM a
	LEFT OUTER JOIN a AS b ON b.col = a.col;`)[1:], sql)
}

func TestChainSwappedCondition(t *testing.T) {
	r := require.New(t)
	// (+) is always left, once for a right join, and once for a left join.
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM a, b, c WHERE a.col(+) = b.col AND c.col(+) = b.col;")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT * FROM a
	RIGHT OUTER JOIN b ON a.col = b.col
	LEFT OUTER JOIN c ON c.col = b.col;`)[1:], sql)
}

func TestRecursedPreserveOrder(t *testing.T) {
	r := require.New(t)
	// The subquery has *, will be a right join.
	sql, err := oracle.Translate("<stdin>", "SELECT 1 FROM (SELECT * FROM a, b WHERE a.col(+) = b.col) a, c WHERE a.col = c.col(+);")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT 1 FROM (SELECT * FROM a
	RIGHT OUTER JOIN b ON a.col = b.col) a
	LEFT OUTER JOIN c ON a.col = c.col;`)[1:], sql)
}

func TestRecursedPreferLeft(t *testing.T) {
	r := require.New(t)
	// The subquery has not *, right join will be edited as a left.
	sql, err := oracle.Translate("<stdin>", "SELECT 1 FROM (SELECT b.col FROM a, b WHERE a.col(+) = b.col) a, c WHERE a.col = c.col(+);")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT 1 FROM (SELECT b.col FROM b
	LEFT OUTER JOIN a ON a.col = b.col) a
	LEFT OUTER JOIN c ON a.col = c.col;`)[1:], sql)
}

func TestInCall(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM depts, users WHERE depts.id = myfunc(users.deptid(+));")

	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT * FROM depts
	LEFT OUTER JOIN users ON depts.id = myfunc(users.deptid);`)[1:], sql)
}

func TestCompositeOn(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT departments.department_name,
		employees.employee_name
	FROM departments, employees
	WHERE departments.department_id = employees.department_id(+)
	AND employees.salary (+) >= 2000
	AND departments.department_id >= 30;
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT departments.department_name,
		employees.employee_name
	FROM departments
	LEFT OUTER JOIN employees ON departments.department_id = employees.department_id
	   AND employees.salary >= 2000
	WHERE departments.department_id >= 30;
	`), sql)
}

func TestCompositeOnRightJoin(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT *
	FROM employees, jobs
	WHERE employees.job_id(+) = jobs.id AND employees.salary(+) > 2000;
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT *
	FROM employees
	RIGHT OUTER JOIN jobs ON employees.job_id = jobs.id AND employees.salary > 2000;
	`), sql)
}

func TestAmbiguous(t *testing.T) {
	r := require.New(t)
	_, err := oracle.Translate("<stdin>", "SELECT * FROM a, b, c WHERE b_id = b.col(+);")
	r.ErrorContains(err, "can't guess table from column")
}

func TestAnonymous(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM depts, emps, (SELECT 1 AS col FROM DUAL) WHERE depts.id = emps.col(+);")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT * FROM depts
	LEFT OUTER JOIN emps ON depts.id = emps.col,
	(SELECT 1 AS col);`)[1:], sql)
}

func TestSpaceless(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT * FROM a,b WHERE a.col=b.col(+)")
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT * FROM a
	LEFT OUTER JOIN b ON a.col = b.col`)[1:], sql)
}

func TestComments(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT *
	FROM a, -- a comment
		-- b comment
		b,
		c
	-- where comment
	WHERE -- condition comment
		a.col = b.col(+);
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT *
	FROM a -- a comment
	-- b comment
	LEFT OUTER JOIN b
	-- where comment
	ON -- condition comment
	       a.col = b.col,
	c;
	`), sql) // condition is on right of the river.
}
