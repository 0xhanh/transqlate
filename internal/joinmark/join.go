package joinmark

import (
	"slices"

	"gitlab.com/dalibo/transqlate/internal/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// guessJoinDirection determines whether we need a LEFT or RIGHT join.
//
// Returns the LEFT or RIGHT keyword.
func guessJoinDirection(right string, on ast.Node) string {
	marked := listMarkedTables(on)
	if slices.Contains(marked, right) {
		// the right side can be null.
		return "LEFT"
	} else {
		// If left table can't be NULL, then it's a RIGHT JOIN.
		return "RIGHT"
	}
}

func buildJoin(direction string, right ast.Node, on ast.Where) ast.Node {
	// Remove join marks.
	condition := on.Visit(func(n ast.Node) ast.Node {
		postfix, _ := n.(ast.Postfix)
		if postfix.Is("(+)") {
			// Drops spacings between expresion and tie-fighter.
			return ast.SpaceFrom(postfix.Expression, postfix)
		}
		return n
	})
	join := ast.Join{
		Join:      lexer.MustLex(" %s OUTER JOIN", direction),
		Right:     right,
		Condition: condition,
	}
	return spaceJoin(join)
}

func spaceJoin(join ast.Join) ast.Join {
	// Move spacings from table to JOIN.
	join = ast.SpaceFrom(join, join.Right)
	// Ensure one space after JOIN keyword.
	// Removes leading comment which was copied by ast.Space.
	join.Right = ast.OneSpace(join.Right)
	start, _ := lexer.Edges(join.Condition)
	if start.LeadingComment() == "" {
		// Put ON after table.
		join.Right = ast.TrimNewLine(join.Right)
		join.Condition = ast.OneSpace(join.Condition)
	} else {
		// Ensure ON is on its line.
		join.Right = ast.EndLine(join.Right)
		join.Condition = ast.IndentFrom(join.Condition, join)
	}
	// Remove final new line before eventual comma.
	// Let ast.From.Space handle newline around table expression.
	join.Condition = ast.TrimNewLine(join.Condition)
	return join
}
