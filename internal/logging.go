package internal

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/lmittmann/tint"
)

var levelStrings = map[string]string{
	// Colors from journalctl.
	"DEBUG": "\033[2mDEBUG ",
	"INFO":  "\033[1mINFO  ",
	"WARN":  "\033[1;38;5;185mWARN  ",
	"ERROR": "\033[1;31mERROR ",
}

func setupLogging() {
	level := slog.LevelInfo
	if verbose {
		level = slog.LevelDebug
	}
	var h slog.Handler
	if plainLog {
		if verbose {
			// plain, verbose: use slog text handler.
			h = slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
				Level: level,
			})
		} else {
			// plain, quiet: use default slog handler.
			return
		}
	} else {
		// colored: use tint handler.
		h = tint.NewHandler(os.Stderr, buildTintOptions(level))
	}
	slog.SetDefault(slog.New(h))
}

func buildTintOptions(level slog.Level) *tint.Options {
	return &tint.Options{
		Level: level,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			// Colorize error, level and message from level.
			switch a.Key {
			case slog.LevelKey:
				a.Value = slog.StringValue(levelStrings[a.Value.String()])
			case slog.MessageKey:
				// Reset color after message.
				a.Value = slog.StringValue(fmt.Sprintf("%-32s", a.Value.String()) + "\033[0m")
			default:
				if a.Value.Kind() == slog.KindAny {
					v := a.Value.Any()
					if nil == v && "err" == a.Key {
						// Drop nil error.
						a.Key = ""
						return a
					}
					// Automatic tint.Err()
					err, ok := v.(error)
					if ok {
						a = tint.Err(err)
					}
				}
			}
			return a
		},
		TimeFormat: "15:04:05",
	}
}
