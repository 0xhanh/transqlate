package render_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/internal/render"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestRenumber(t *testing.T) {
	r := require.New(t)

	// Single line
	first := lexer.MustLex("SELECT")[0]
	tokens := lexer.MustLex(" * FROM t1")
	tokens = render.Renumber(first, tokens)
	r.Equal(0, tokens[0].Line)
	r.Equal(7, tokens[0].Column)
	r.Equal(0, tokens[1].Line)
	r.Equal(9, tokens[1].Column)

	// Multiline
	first = lexer.MustLex("\nSELECT\n")[0]
	tokens = render.Renumber(first, tokens)
	r.Equal(2, tokens[0].Line)
	r.Equal(1, tokens[0].Column)
}
