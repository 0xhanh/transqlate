package lexer

import (
	"strings"
)

type Tokener interface {
	Tokens() []Token
}

// Edges returns the first and last non-void tokens.
func Edges(tokener Tokener) (start Token, end Token) {
	if tokener == nil {
		return
	}
	for _, t := range tokener.Tokens() {
		if t.Type == Void {
			continue
		}
		if start.Type == Void && start.Type != EOF {
			start = t
		}
		end = t
	}
	return
}

// Write returns the string representation of the tokens.
func Write(n Tokener) string {
	if n == nil {
		return ""
	}
	b := strings.Builder{}
	for _, t := range n.Tokens() {
		if t.Type == Void {
			continue
		}
		b.WriteString(t.Prefix)
		b.WriteString(t.Raw)
		b.WriteString(t.Suffix)
	}
	return b.String()
}

// ShortWrite represent a token list in a short form.
//
// No new line. No comment. No indentation. Shortened to 50 characters. An ellipsis is appended if
// the output is shortened.
//
// Useful for logging.
func ShortWrite(n Tokener) string {
	end := ""
	if n == nil {
		return ""
	}
	var tokens Tokens
	for _, t := range n.Tokens() {
		if t.Type == Void {
			continue
		}
		if len(tokens) == 0 {
			t.Prefix = "" // Remove leading spaces
		} else if strings.Contains(t.Prefix, "\n") {
			end = "..." // Stop after newline.
			break
		}

		if t.LeadingComment() != "" {
			t.Prefix = t.Indentation()
		}

		if t.EndsLine() {
			t.Suffix = t.Suffix[:strings.Index(t.Suffix, "\n")]
			tokens = append(tokens, t)
			end = "..."
			break
		} else {
			tokens = append(tokens, t)
		}
	}

	out := Write(tokens)
	if len(out) > 50 {
		out = out[:50]
		end = "..."
	}

	return out + end
}

// Tokens is a list of tokens implementing lexer.Tokener.
//
// Cast a slice of Token to Tokens to implement lexer.Tokener interface or use Tokens methods.
type Tokens []Token

func (t Tokens) Tokens() []Token {
	return t
}

// LineAt returns the SQL line of the token at pos.
//
// pos is the position of the token in the list.
func (tokens Tokens) LineAt(pos int) Tokens {
	l := tokens[pos].Line
	var lineTokens Tokens

	// Prepend previous token on the same line
	for i := pos - 1; i >= 0; i-- {
		t := tokens[i]
		if t.Line != l {
			break
		}
		lineTokens = append([]Token{t}, lineTokens...)
	}

	// Append next token on the same line
	for i := pos; i < len(tokens); i++ {
		t := tokens[i]
		if t.Line != l {
			break
		}
		lineTokens = append(lineTokens, t)
	}

	return lineTokens
}
