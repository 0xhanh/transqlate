package rewrite_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/rewrite"
)

func TestUpperKeyword(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("select 1 from t1;")
	e := rewrite.Engine{}
	e.Apply(rewrite.Pretty())
	out, err := e.Run(n)
	r.NoError(err)
	r.Equal("SELECT 1 FROM t1;", lexer.Write(out))
}

func TestLowerIdentifiers(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse(`"upper"(A) + "select"`)
	e := rewrite.Engine{}
	e.Apply(rewrite.Pretty())
	out, err := e.Run(n)
	r.NoError(err)
	r.Equal(`upper(a) + "select"`, lexer.Write(out))
}
