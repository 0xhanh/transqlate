package rewrite

import (
	"fmt"
	"os"

	"gitlab.com/dalibo/transqlate/internal/ast"
)

var debug bool // if true, don't wrap rule panic. Set this with DEBUGRULES=1 env.

func init() {
	debug = os.Getenv("DEBUGRULES") != ""
}

// wrapPanic calls the given function and returns the panic as an error.
func wrapPanic(f func()) (err error) {
	if debug {
		f()
		return
	}
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("panic: %v", r)
		}
	}()
	f()
	return
}

// Err holds context for a rewriting failure.
//
// [Engine.Run] returns a list of [Err].
type Err struct {
	Err  error    // Wrapped error
	Node ast.Node // ast.Node under transformation
	Rule any      // Rule that triggered the error
}

func (e Err) Error() string {
	return e.Err.Error()
}

func (e Err) Unwrap() error {
	return e.Err
}
