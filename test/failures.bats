#!/usr/bin/env bats

@test "lexer error colored" {
	run transqlate --plain=false <<<"SELECT 'unterminated"
	[ "$status" -eq 1 ]
	[[ "${output}" == *"unterminated string literal"* ]]
}

@test "lexer error plain" {
	run transqlate --plain <<<"SELECT 'unterminated"
	[ "$status" -eq 1 ]
	[[ "${output}" == *"^^^"* ]]
}

@test "parser error colored" {
	run transqlate --plain=false <<<"SELECT CREATE"
	[ "$status" -eq 1 ]
	[[ "${output}" == *"unexpected"* ]]
}

@test "parser error plain" {
	run transqlate --plain <<<"SELECT CREATE"
	[ "$status" -eq 1 ]
	[[ "${output}" == *"^^^^"* ]]
}

@test "rule failure" {
	run transqlate --plain=false ./test/fixtures/partial-failure.sql
	[ "$status" -eq 1 ]
	[[ "${output}" == *"TRANSLATION ERROR"* ]]
}
